use JSON::Tiny;
use Digest::SHA1::Native;

unit module Tarea;

enum PrefixStatus is export <Unknown Ambiguous>;

# Represents a task.
class Task {
    has Str  $.name   is rw;
    has Str  $.id     is rw;
    has Str  $.prefix is rw;
}

# Compose subtasks into a given task.
role Subtask {
    has Task %.subtasks;

    method add-subtask( Task $task ) {
        %!subtasks{ $task.id } = $task;
    }
}

#|«
A group of tasks, both finished and unfinished, for a given list. The file
holding the tasks is read during a TaskList object initialization. To write
any modification of the list of task, use the save() method.
»
class TaskList is export {
    has $!file = '.tareas.json'; # JSON file where tasks are stored
    has %!unfinished;            # stores unfinished tasks
    has %!done;                  # stores finished tasks

    submethod TWEAK {
        $!file = $*HOME.add: $!file;

        # If file exists, read it and store the tasks in %!unfinished ...
        if $!file.IO.e and $!file.IO.f {
            my %json = from-json($!file.slurp);
            for %json.keys -> $type {
                for %json{$type}.keys -> $id {
                    my $task = Task.new: name => %json{$type}{$id}<name>, :$id;

                    # if task has subtasks...
                    if %json{$type}{$id}<subtasks> {
                        my %subtasks = %json{$type}{$id}<subtasks>;
                        $task does Subtask;
                        for %subtasks.keys -> $id {
                            $task.add-subtask: Task.new:
                                :name(%subtasks{$id}<name>),
                                :$id;
                        }
                    }

                    if    $type eq 'unfinished' { %!unfinished{$task.id} = $task }
                    elsif $type eq 'done'       { %!done{$task.id}  = $task }
                }
            }
        }
        # ... Else prompt the user for the file creation.
        else {
            my $create-file = prompt 'Do you want to create the file? (y/n) ';
            put "Using '$!file' as default file.";
            my $change-default = prompt 'Do you want to change it? (y/n) ';

            if $change-default.lc.starts-with('y') {
                $!file = (prompt 'Enter name of file: ').comb(/\w+/) ~ '.json'
            }

            if $create-file.lc.starts-with('y') {
                $!file = $*HOME.add: $!file unless $!file ~~ IO::Path;
                $!file.spurt: '{}';
            }
            put "File was created: $!file" if $!file.e;
        }
    }

    #| Add a new task. By default, tasks don't have subtasks.
    method add-task( Str:D $name ) {
        my $id   = self!hash-text($name);
        my $task = Task.new: :$name, :$id;
        %!unfinished{ $id } = $task;
    }

    #| Add subtask to task with prefix (ID).
    method add-subtask( $tasklist : $task-id, Str:D $name ) {
        my $task = $tasklist{$task-id};
        return $task if $task ~~ PrefixStatus;

        my $subtask = Task.new: :$name, :id(self!hash-text($name));
        if $task.does(Subtask) {
            $task.add-subtask: $subtask;
        }
        else {
            $task does Subtask;
            $task.add-subtask: $subtask;
        }
    }

    #|«
    Remove and return task with prefix. Otherwise, return a PrefixStatus
    depending on whether no tasks match prefix or more than one task match it.
    »
    method remove-task( $tasklist : Str:D $prefix ) {
        my $task = $tasklist{ $prefix };
        return $task if $task ~~ PrefixStatus;
        return $tasklist{ $prefix }:delete;
    }

    #|«
    Edit task with prefix. Otherwise, return a PrefixStatus depending
    on whether no tasks match prefix or more than one task match it.
    »
    method edit-task( $tasklist : Str:D $prefix, Str:D $text ) {
        my $task = $tasklist{ $prefix };
        return $task if $task ~~ PrefixStatus;
        $task.name = $text;
        $task.id   = self!hash-text: $text;
    }

    #|«
    Edit subtasks with prefix of task with prefix. Otherwise, return a
    PrefixStatus depending on whether no tasks match prefix or more than one
    task match it.
    »
    method edit-subtask( $tasklist : Str:D $task-id, Str:D $subtask-id, Str:D $text) {
        my $task      = $tasklist{ $task-id };
        my $subtask   = get-task-with($task.subtasks, $subtask-id);
        return PrefixStatus if PrefixStatus ~~ ($task | $subtask);
        $subtask.name = $text;
        $subtask.id   = self!hash-text: $text;
    }

    #|«
    Mark a task with prefix (ID) as finished. Otherwise return a PrefixStatus
    depending on whether no tasks match prefix or more than one task match it.
    »
    method finish-task( $tasklist : Str:D $prefix )  {
        my $task = $tasklist{ $prefix };
        return $task if $task ~~ PrefixStatus;
        %!done{ $task.id } = $task;     # Add task to %!done and ...
        $tasklist.remove-task: $prefix; # ... remove it from %!unfinished
    }

    #| Return a list of finished tasks.
    method get-finished-tasks   ( --> List ) { %!done.values.list }
    #| Remove all finished tasks.
    method remove-finished-tasks( --> Nil  ) { %!done = Empty     }

    #|«
    Write finished and unfinished tasks to disk.
    »
    method save( --> Nil ) {
        my %pre-json;

        for %!unfinished.keys -> $id {
            my %task-json;
            %task-json<id>   = $id;
            %task-json<name> = %!unfinished{$id}.name;

            if %!unfinished{$id}.does(Subtask) {
                for %!unfinished{$id}.subtasks -> $st {
                    %task-json<subtasks>{$st.key} = %(name => $st.value.name);
                }
            }
            %pre-json<unfinished>{$id} = %task-json;
        }

        %pre-json<done> = %!done.keys.map({
            $_ => %( name => %!done{$_}.name )
        }).hash;
        $!file.spurt: to-json %pre-json;
    }

    #| Print a formatted list of unfinished tasks.
    method print-list(
        Str $kind where * ∈ <unfinished done> = 'unfinished', # Type of tasks: unfinished and done
        Int $number = 5,                            # Number of tasks to display
        Str  :$grep = '',                           # String to grep with
        Bool :$verbose = False,                     # Use either prefix or id
        Bool :$quiet = False,                       # Hide id/prefix
        --> Nil
    ) {
        my %tasks = do given $kind {
            when 'unfinished' { %!unfinished }
            when 'done'       { %!done  }
        }

        unless %tasks {
            put "No tasks.";
            exit;
        }

        # add task's prefix when not verbose is used
        unless $verbose {
            self!prefixes(%tasks.keys).kv.map: -> $id, $prefix {
                %tasks{$id}.prefix = $prefix;
            }
        }

        # get length of the longer identifier (be it id or prefix)
        # in the task list
        my $label = $verbose ?? 'id' !! 'prefix';
        my $max-label = do if %tasks.values {
            given $label {
                when 'prefix' { %tasks.values».prefix».chars.max }
                when 'id'     { %tasks.values».id».chars.max     }
            }
        } else { 0 }

        for %tasks.values.sort({ .name.tc }).head($number) -> $task {
            if $task.name.lc.contains($grep.lc) {
                my $identifier = do given $label {
                    when 'prefix' { $task.prefix }
                    when 'id'     { $task.id     }
                }
                my $sep = ' - ';
                ($max-label, $identifier, $sep) = (0, '', '') if $quiet;
                printf "%-*s%s%s\n", $max-label, $identifier, $sep, $task.name.tc;
            }
        }
    }

    #| Print a formatted list of a task's subtasks.
    method print-subtasks( $tasklist :
        $prefix,
        Bool :$verbose = False,
        Bool :$quiet = False
        --> Nil
    ) {
        my $task = $tasklist{ $prefix };
        if $task.does(Subtask) && $task.subtasks {
            # add prefixes to task if not verbose
            unless $verbose {
                for $task.subtasks.values -> $st {
                    # add prefix to subtask
                    $st.prefix = self!prefixes([$st.id]).values.first;
                }
                # add prefix to task
                $task.prefix = self!prefixes([$task.id]).values.first;
            }
            # get length of the longer identifier (be it id or prefix)
            # in the task list
            my $label = $verbose ?? 'id' !! 'prefix';
            my $max-label = do if $task.subtasks {
                given $label {
                    when 'prefix' { $task.subtasks.values».prefix».chars.max }
                    when 'id'     { $task.subtasks.values».id».chars.max     }
                }
            } else { 0 }

            my $t = $task.prefix ~ " " ~ $task.name;
            put $t, "\n", '‒' x $t.chars;

            for $task.subtasks.values.sort({ .name.tc }) -> $st {
                my $identifier = do given $label {
                    when 'prefix' { $st.prefix }
                    when 'id'     { $st.id     }
                }
                my $sep = ' - ';
                ($max-label, $identifier, $sep) = (0, '', '') if $quiet;
                printf "   %-*s%s%s\n", $max-label, $identifier, $sep, $st.name.tc;
            }
        }
        else {
            put "No subtasks for task '$prefix'."
        }
    }

    =comment START Private methods

    #`«
    Get the SHA1 hash of the text.
    »
    method !hash-text( Str:D $text --> Str ) {
        sha1-hex($text)
    }

    #`«
    Return a mapping of ids to prefixes.
    »
    method !prefixes( @ids --> Hash ) {
        my %prefixes;
        for @ids -> $id {
            my $prefix;
            my $i;
            loop ($i = 1; $id.chars+1; $i++) {
                $prefix = $id.substr(0, $i);
                last if %prefixes{$prefix}:!exists or
                        %prefixes{$prefix}:exists && $prefix ne %prefixes{$prefix};
            }

            # prefix still isn't mapped to an id so map this prefix to id
            if %prefixes{$prefix}:!exists {
                %prefixes{ $prefix } = $id
            }

            # prefix is already mapped to another id so there's a collision
            else {
                my $other-id = %prefixes{$prefix};
                my Bool $is-substring;

                loop (my $j = $i; $j < $id.chars; $j++) {
                    if $other-id.substr(0, $j) eq $id.substr(0, $j) {
                        %prefixes{ $id.substr(0, $j) } = '';
                        $is-substring = True;
                    }
                    else {
                        # Subtrings of IDs were unique enough so use them
                        # as prefixes for their respective IDs.
                        %prefixes{ $other-id.substr(0, $j) } = $other-id;
                        %prefixes{ $id      .substr(0, $j) } = $id;
                        $is-substring = False;
                        last;
                    }
                }

                # The ID of a task is entirely a substring of another task's ID,
                # so use entire first id as a prefix.
                if $is-substring {
                    %prefixes{ $other-id.substr(0, $id.chars+1) } = $other-id;
                    %prefixes{ $id } = $id;
                }
            }
        }

        %prefixes .= invert;
        %prefixes{''}:delete if %prefixes{''}:exists;
        return %prefixes;
    }

    =comment END Private methods

    =begin comment
    NOTE: The following methods only access the unfinished tasks (%!unfinished).
    To access finished tasks, use the get-finished-task() method. To remove
    them all, use remove-finished-tasks().

    With a TaskList $tl object, these methods allow the following:

    my TaskList $tl .= new;

    $tl<cd>;          # Retrieve task with id 'cd'. Either return the task
                      # or PrefixStatus.
    $tl<cd>:exists;   # Does a task with id 'cd' exist?
    $tl<cd>:delete;   # Delete task with id 'cd'.
    =end comment

    #`«
    Return the unfinished task with the given prefix.
    Return Unknown if no tasks match the prefix.
    Return Ambiguous if more than one task match the prefix.
    »
    sub get-task-with( %tasks, $prefix ) {
        # get ids that match the prefix.
        my @matched-prefixes = %tasks.keys.grep: *.starts-with($prefix);

        return do given @matched-prefixes {
            when .elems == 0 { Unknown }
            when .elems == 1 { %tasks{ @matched-prefixes[0] } }
            default          {
                my @matched = %tasks.keys.grep: $prefix;
                @matched.elems == 1 ?? %tasks{ @matched[0] } !! Ambiguous;
            }
        }
    }

    multi method AT-KEY( ::?CLASS:D: $prefix ) {
        get-task-with(%!unfinished, $prefix)
    }

    multi method EXISTS-KEY( ::?CLASS:D $tasklist: $prefix ) {
        my $task = $tasklist{ $prefix };
        return False if $task ~~ PrefixStatus;
        return True  if $task ~~ Task;
    }

    multi method DELETE-KEY( ::?CLASS:D $tasklist: $prefix ) {
        my $task = $tasklist{ $prefix };
        return Any if $task ~~ PrefixStatus;
        return %!unfinished{ $task.id }:delete;
    }

}
