NAME
====



`tarea` - A simple CLI todo list manager written in Raku and inspired by Steve Losh's [t](http://stevelosh.com/projects/t/).

INSTALLATION
============



Using zef:
----------

    zef update && zef install tarea

From source:
------------

    git clone git@gitlab.com:uzluisf/raku-tarea.git
    cd raku-tarea && zef install .

CLI
===



Adding a task
-------------

To add a task, use `tarea -a` followed by the task description.

    $ tarea -a 'Water the flowers.'
    $ tarea -a 'Finish writing the English essay.'
    $ tarea -a 'Start doing the Math homework.'

Listing the tasks.
------------------

To list the task, just run `tarea`. By default only the first 10 unfinished tasks are displayed.

    $ tarea
    9 - Finish writing the English essay.
    6 - Start doing the Math homework.
    a - Water the flowers.

Adding a subtask to a task
--------------------------

To add a subtask to a given task, use `tarea -a <ID> <task>`, where `ID` is the task's ID and `task` is the subtask's description. Make sure to surround both fields with quotes.

    $ tarea -a '9' 'Start with the introduction.'
    $ tarea -a '9' 'Expand main in body.'

Displaying a task's subtasks
----------------------------

To display a task's subtasks, use `tarea -s <ID>`.

    $ tarea -s '9'
    9 Finish writing the English essay.
    ‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
       7 - Expand main in body.
       c - Start with the introduction.

Editing a task
--------------

To edit a task's description, run `tarea -e <ID>`, following by the task's new description.

    $ tarea -e 'a' 'Water the flowers. Also prune the bonsai tree.'
    $ tarea
    9 - Finish writing the English essay.
    6 - Start doing the Math homework.
    a - Water the flowers. Also prune the bonsai tree.

Editing a subtask
-----------------

To edit a subtask's description, run `tarea -e <TASKID> <SUBTASKID>`, followed by the subtask's new description.

    $ tarea -e '9' '7' 'Expand main idea in the body.'
    $ tarea -s '9'
    9 Finish writing the English essay.
    ‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
       7 - Expand main idea in the body.
       c - Start with the introduction.

Marking a task as finished
--------------------------

To mark a task as finished, run `tarea -f <ID>`.

    $ tarea -f '9'
    $ tarea
    6 - Start doing the Math homework.
    a - Water the flowers. Also prune the bonsai tree.

List finished tasks.
--------------------

To list all finished tasks, run `tarea -k=done`.

    $ tarea -kind=done
    9 - Finish writing the English essay.

Removing a task
---------------

To remove a task, run `tarea -r <ID>`.

    $ tarea -r '6'
    $ tarea
    a - Water the flowers. Also prune the bonsai tree.

Removing all finished tasks
---------------------------

To remove all unfinished tasks, run `tarea -r finished`.

    $ tarea -r finished
    $ tarea -k=done
    No tasks.

Displaying the help message
---------------------------

To display the help message, run `tarea -h`.

PROGRAM
=======



`tarea` is just a front-end for the module `Tarea`, which can be imported into a Raku script. By doing so the class `TaskList`, which is the heart of the module, is exposed.

    use Tarea;

    my TaskList $tl .= new;
    my $task = "Listen to Hello Meteor's new album";

    given $tl {
        .add-task: $task;
        .save;
        .<1>.name.put;   #=> Listen to Hello Meteor's new album
        .<1>:exists.put; #=> True
    }

METHODS
-------

  * `method add-task(Str:D $name,)` - Add a new task. By default, tasks don't have subtasks.

  * `method add-subtask($task-id, Str:D $name,)` - Add subtask to task with prefix (ID).

  * `method remove-task(Str:D $prefix,)` - Remove and return task with prefix. Otherwise, return a PrefixStatus depending on whether no tasks match prefix or more than one task match it.

  * `method edit-task(Str:D $prefix, Str:D $text,)` - Edit task with prefix. Otherwise, return a PrefixStatus depending on whether no tasks match prefix or more than one task match it.

  * `method edit-subtask(Str:D $task-id,Str:D $subtask-id,Str:D $text,)` - Edit subtasks with prefix of task with prefix. Otherwise, return a PrefixStatus depending on whether no tasks match prefix or more than one task match it.

  * `method finish-task(Str:D $prefix,)` - Mark a task with prefix (ID) as finished. Otherwise return a PrefixStatus depending on whether no tasks match prefix or more than one task match it.

  * `method get-finished-tasks()` - Return a list of finished tasks.

  * `method remove-finished-tasks()` - Remove all finished tasks.

  * `method save()` - Write finished and unfinished tasks to disk.

  * `method print-list(Str $kind, Int $number, Str :$grep, Bool :$verbose, Bool :$quiet)` - Print a formatted list of unfinished tasks.

  * `method print-subtasks($prefix, Bool :$verbose, Bool :$quiet)` - Print a formatted list of a task's subtasks.

AUTHOR
======

Luis F. Uceta

LICENSE
=======

Artistic License 2.0

